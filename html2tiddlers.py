'''
This script converts chat transcripts with Google's Gemini large language model into JSON data that can be imported into TiddlyWiki 5.

To use this script, save your Gemini chat as a single HTML webpage by saving the page from your web browser. Then run this script from the command line. Then import the resulting JSON file into your TiddlyWiki. Every message will be an individual tiddler, whether a prompt from the human user or a response from the AI.
'''

# Saturday June 15, 7:25 p.m. EST

# Step 1: Include the HTML parser; declare any classes and globals.

import os
import sys
import json
import argparse
from datetime import datetime
from bs4 import BeautifulSoup

class Conversation:
    chatbot = 'gemini'           # Could hypothetically be expanded to support other chat platforms
    input_source = 'saved_html'  # Could be expanded to support other input types—i.e., scraping autmatically by URL, copy/paste transcripts, etc.
    # number of turns
    # number of human prompts
    # numbe of AI responses
    # def __init__():

class Message:
    def __init__(self, title, tags, fields, content, timestamp):
        self.title = title            # either a string or a method to make the string
        self.fields = fields          # a dictionary
        self.content = content        # a string containing the wikitext
        self.tags = tags              # a list of strings
        self.timestamp = timestamp    # a string or a method to create and format the timestamp
        self.number = number          # position in conversation sequence
        self.next_tiddler = next      # reference to tiddler with the next self.number value
        self.previous_tiddler = prev  # reference to tiddler with the previous self.number value
        self.turn = turn              # an identifier grouping both a prompt and a response
        self.source = source          # either human written or AI generated ('human' or 'ai')
    # for debugging:
    def __repr__(self):
        return (
            f"Tiddler(title={self.title}, fields={self.fields}, tags={self.tags}, timestamp={self.timestamp}, number={self.number}, next_tiddler={self.next_tiddler}, previous_tiddler={self.previous_tiddler}, turn={self.turn}, source={self.source}, content={self.content[:30]}, )"
        )

sequence = -1  # a global counter, used for generating class properties; value of -1 means the script hasn't tried to iterate yet

# Step 2: The control loop -- conditionals to test for arguments during command-line execution; main() function.

def main(conversation, json_output):
    print("Hey, we're going to do stuff but not yet.")
    dom = BeautifulSoup(conversation, 'html.parser')
    dom_title = dom.find('title')
    print(f"Title of webpage: {dom_title}")
    wrapper = dom.find(class_='conversations-container')
    if wrapper:
        conv_title = wrapper.find(class_='conversation-title').strip()
        if conv_title:
            print(f"Title of conversation: {conv_title}")
        else:
            print("Title not found.")
    else:
        print("Wrapper not found.")

def write_json(data_input, file_output):
    """
    Writes JSON data to a file.

    Args:
    - data_input (dict or list): The data to be serialized to JSON and written to the file.
    - file_output (string)     : The path where the JSON file should be saved.
    """
    try:
        with open(file_output, 'w', encoding='utf-8') as f:
            json.dump(data_input, f, indent=4, ensure_ascii=False)
        print(f"Data successfully written to {file_output}")
    except IOError as e:
        print(f"Error writing to file: {str(e)}")


if __name__ == "__main__": # If this script is being directly executed from the CLI, which is the intended use
    parser = argparse.ArgumentParser(description="Select HTML file to convert.")
    parser.add_argument("html_file", help="The HTML file saved from a Gemini conversation")
    parser.add_argument("json_file", help="The JSON file being output to be imported into TiddlyWiki.")
    args = parser.parse_args()
    # Checking whether the command line argument correctly specifies the file
    if args.html_file and args.json_file:
        if os.path.isfile(args.html_file):
            try:
                with open (args.html_file, 'r', encoding='utf-8') as file:
                    main(file.read(), args.json_file) # ...move on to the rest of the script
            except FileNotFoundError:
                parser.error("The specified HTML file wasn't found.")
            except UnicodeDecodeError:
                parser.error("The HTML file wasn't readable using UTF-8 encoding.")
            except PermissionError:
                print(f"The file '{args.html_file}' is inaccessible due to permissions.")
            finally:                 # Run at the end of the script
                print("The end of the controlling conditional has been reached.")
                sys.exit()           # quit this script
        else:
            parser.error("The first argument must specify an HTML file in the current directory.")
            sys.exit()               # quit this script
    else:
        parser.error("2 arguments required:\r\t- HTML file in the present directory\r\t- JSON file to be created in the current directory.")

# Step 3: Parse HTML to find message data.



# Step 4: Create representation of data model for tiddlers.



# Step 5: Parse output argument, determining output JSON file. Additional CLI nodes.



# Step 6: Perhaps based on parsed CLI arguments, determine how internal HTML in data will be rendered.



# Step 7: Output status, results, and debugging information to command line.
